﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



// Имеем класс "Команды"
// Этот класс знает за кого мы играем, хранит эту информацию, и видимо передает команде
// Как я понимаю мы должны уметь создавать команды
// Следовательно жизненный цикл команды следующий.
// Создается новый экземпляр класса какой либо команды (соответственно нужен класс под каждую команду со свойей реализацией интерфеса)
// Класс который разруливает комманды, принимает в аргуметы созданную команду и выполняеет ее
// При появлении пользовательского интерфейса он и будет создавать новые экземпляры команд
// Экземпляры комманд реализуют один интерфейс, (рефакторинг гуру там текстовый редактор, нам подходит)
// Информация о получателе команды и о параметрах передаваемых бизнес логике хранится в полях комманды
// (Кажется команда знает про фракцию и ранг как информация о параметрах с которыми должен быть вызван метод получателя и гейм контекст в роли получателя)
// Раз у нас уже есть gamecontext, пусть команды в гейм контекст и добавляют игроков.
// Скорее некоторые команды должны знать про игровое время, что бы показывать прогресс и знать когда действительно вводить новых юнитов в игру

namespace towerDefense
{
    public abstract class Command
    {
        protected IGameContext _gameContext;
        protected Fraction _fraction;

        public Command(IGameContext gameContext, Fraction fraction)
        {
            _gameContext = gameContext;
            _fraction = fraction;
        }
        public abstract void Execute();
    }

    public class AddMobileUnit : Command
    {
        private Rank _rank;
        public AddMobileUnit(IGameContext gameContext, Fraction fraction, Rank rank)
            : base(gameContext, fraction) 
        {
            _rank = rank;
        }

        public override void Execute()
        {
            _gameContext.AddUnit(new MobileUnit(_fraction, _rank, _fraction.Direction));
        }
    }
}
