﻿using System.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace towerDefense
{
    class DeferredCommand
    {
        private Command _command;
        public Command Command { get => _command; }
        private TimeSpan _accumulatedTime;
        private TimeSpan _leadTime;
        private bool _canExecute;
        public bool CanExecute { get => _canExecute; }

        public DeferredCommand(Command command, TimeSpan leadTime)
        {
            _command = command;
            _leadTime = leadTime;
            _accumulatedTime = default;
            _canExecute = false;
        }

        public void AddAccumulatedTime(TimeSpan timeSpan)
        {
            _accumulatedTime = _accumulatedTime.Add(timeSpan);
            if (_accumulatedTime >= _leadTime)
            {
                _canExecute = true;
            }
        }
        



    }
}
