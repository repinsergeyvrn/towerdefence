﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace towerDefense
{
    public class MobileUnit : AttackUnit, IMobile
    {
        //private int _cost = 500;
        private Point _direction;
        private double _speed = 1;
        private double _currentSpeed;
        public double Speed { get => _currentSpeed; 
                              set
                              {
                                  _speed = value;
                                  _currentSpeed = value;
                              }
                            }

        public MobileUnit(Fraction fraction, Rank rank)
        {
            _fraction = fraction;
            _rank = rank;

            Fraction.ApplayCharacteristics(this);
            base.AplayRank();
            _damage = (int)Math.Round(_rank.DamageCoefficient * _damage, MidpointRounding.AwayFromZero);
            _speed *= _rank.SpeedCoefficient;
        }

        public MobileUnit(Fraction fraction, Rank rank, Point direction)
            : this(fraction, rank)
        { 
            _direction = direction;
        }

        public MobileUnit(Fraction fraction, Rank rank, Point direction, double speed)
            : this(fraction, rank, direction)
        {
            _speed = speed;
            _currentSpeed = speed;
        }


        public override string Info()
        {
            string[] infoValues = new string[] { _position.ToString(), _health.ToString(), _fraction.ToString(), _rank.ToString(), _currentSpeed.ToString()};
            return String.Join(" ", infoValues);
        }

        public override void ObjectUpdate(TimeSpan timeSpan, IGameContext gameContext)
        {
            CalculateDamage(gameContext);
            CalculateCollision(gameContext);
            if (_currentSpeed > 0)
            {
                CalculatePosition(timeSpan, gameContext); 
                gameContext.Console.IAmUpdate((Unit)this);
            }
            _betweenAttak = _betweenAttak.Add(timeSpan);
            _canAttack = _betweenAttak > _timeAttac ? true : false;
            Attack();

        }

        public void CalculateCollision(IGameContext gameContext)
        {
            bool wasCollision = false;
            _attackUnit.Clear();
            foreach (var item in gameContext.AllUnits)
            {
                if (item.Fraction.Equals(Fraction))
                {
                    continue;
                }

                double distance = Position.Distance(this.Position, item.Position);
                if (distance <= CollisionDistance)
                {
                    _currentSpeed = 0;
                    wasCollision = true;
                }
                if (_canAttack && distance <= _attackDistance)
                {
                    _attackUnit.Add((AttackUnit)item);
                }

            }
            if (!wasCollision)
            {
                _currentSpeed = _speed;
            }

        }

        public void CalculatePosition(TimeSpan timeSpan, IGameContext gameContext)
        {
            Position curentPosition = _position.Copy();
            Position newPosition = _position.Add(_direction.X * timeSpan.TotalSeconds * _currentSpeed, _direction.Y * timeSpan.TotalSeconds * _currentSpeed);
            Unit nearHostileUnit = gameContext.NearHostileUnit(_fraction, _direction);

            gameContext.UpdatePositionRemove(this);
            if ((nearHostileUnit != null) && (Position.Distance(curentPosition, newPosition) > Position.Distance(curentPosition, nearHostileUnit.Position)))
            {
                _position.X = (nearHostileUnit.Position.X + (_collisionDistance * (_direction.X * -1)));
            }
            else
            {
                _position = newPosition;
            }
            gameContext.UpdatePositionAdd(this);
        }
    }
}
