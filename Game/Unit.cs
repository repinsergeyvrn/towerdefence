﻿using System;
using System.Collections.Generic;

namespace towerDefense
{
    public abstract class Unit : IUnit
    {
        protected Fraction _fraction;
        protected Rank _rank;

        protected int _health = 100;
        protected int _armor = 0;
        protected double _collisionDistance = 1;
        protected Position _position = new Position(0, 0);
        //static public Subscriber _console;
       
        public Fraction Fraction { get => _fraction; }
        public Rank Rank { get => _rank; }

        //public List<Unit> AllUnits { get => _allUnits; set => _allUnits = value; }
        public Position Position { get => _position; set => _position = value;  }
        public int Health { get => _health; set => _health = value;}
        public int Armor { get => _armor; set => _armor = value; }
        protected double CollisionDistance { get => _collisionDistance; set => _collisionDistance = value; }

        public abstract void ObjectUpdate(TimeSpan timeSpan, IGameContext gameContext);

        public void  AplayRank()
        {
            _health = (int)Math.Round(_rank.HealthCoefficient * _health, MidpointRounding.AwayFromZero);
            _armor = (int)Math.Round(_rank.HealthCoefficient * _armor, MidpointRounding.AwayFromZero);
        }

        public virtual string Info()
        {
            string[] infoValues = new string[] { _position.ToString(), _health.ToString(), _fraction.ToString()};
            return  String.Join(" ", infoValues);
        }

    }
}
