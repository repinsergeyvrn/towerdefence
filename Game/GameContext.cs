﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace towerDefense
{
    public class GameContext : IGameContext
    {
        private List<Unit> _allUnits;
        public List<Unit> AllUnits { get => _allUnits; }
        public List<AvailableUnit> AvailableUnits { get; set; }

        public Subscriber Console { get; set; }

        private List<DeferredCommand> _deferredCommand;
        internal List<DeferredCommand> DeferredCommand { get => _deferredCommand; }

        public List<Fraction> Fractions { get; set; }
        private Dictionary<Fraction, SortedSet<Unit>> _hostileUnits;

        public int Bank { get; set; }

        public GameContext()
        {
            _allUnits = new List<Unit>();
            _deferredCommand = new List<DeferredCommand>();
            _hostileUnits = new Dictionary<Fraction, SortedSet<Unit>>();
            AvailableUnits = new List<AvailableUnit>();
            Bank = 100000;
            //Распределить по враждебным юнитам
        }

        public void AddUnit(Unit unit)
        {
            UpdatePositionAdd(unit);
            _allUnits.Add(unit);
        }

        public void RemoveUnit(Unit unit)
        {
            UpdatePositionRemove(unit);
            _allUnits.Remove(unit);
        }

        public void UpdatePositionAdd(Unit unit)
        {
            SortedSet<Unit> foundValue;
            if (_hostileUnits.TryGetValue(unit.Fraction, out foundValue))
            {
                foundValue.Add((Unit)unit);
            }
            else
            {
                SortedSet<Unit> HostileUnits = new SortedSet<Unit>(new UnitComparer());
                HostileUnits.Add(unit);
                _hostileUnits.Add(unit.Fraction, HostileUnits);
            }
        }

        public void UpdatePositionRemove(Unit unit)
        {
            SortedSet<Unit> foundValue;
            if (_hostileUnits.TryGetValue(unit.Fraction, out foundValue))
            {
                foundValue.Remove(unit);
            }
        }

        public List<SortedSet<Unit>> HostileUnits(Fraction ownFraction)
        {
            return (from keyValue in _hostileUnits
                    where keyValue.Key != ownFraction
                    select keyValue.Value).ToList();
        }

        public virtual Unit NearHostileUnit(Fraction ownFraction, Point ownDirection)
        {
            SortedSet<Unit> NearUnitsFractions = new SortedSet<Unit>();
            foreach (KeyValuePair<Fraction, SortedSet<Unit>> keyValue in _hostileUnits)
            {
                if (keyValue.Key != ownFraction)
                {
                    NearUnitsFractions.Add(GetMinMax(keyValue.Value, ownDirection));
                }
            }

            return GetMinMax(NearUnitsFractions, ownDirection);
        }

        private Unit GetMinMax(SortedSet<Unit> SS, Point direction)
        {
            if (direction.X == 1)
            {
                return SS.Min;
            }
            else
            {
                return SS.Max;
            }
        }

        public int RemoveAll(Predicate<Unit> match)
        {

            foreach (var value in _hostileUnits.Values)
            {
                value.RemoveWhere(match);
            }
            return _allUnits.RemoveAll(match);
        }

        public void AddDeferredCommand(Command command, TimeSpan leadTime)
        {
            _deferredCommand.Add(new DeferredCommand(command, leadTime));
        }

        public void BuyUnit(int numbers)
        {
            int cost = AvailableUnits[numbers].Cost;
            if (cost <= Bank)
            {
                AvailableUnits[numbers].Command.Execute();
                Bank -= cost;
            }
        }

    }
}
