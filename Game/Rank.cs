﻿using System;

namespace towerDefense
{
    public class Rank
    {
        string _name;
        public double HealthCoefficient { get; }
        public double DamageCoefficient { get; }
        public double ArmorCoefficient { get; }
        public double SpeedCoefficient { get; }

        public Rank(string name, double improvementRate)
        {
            _name = name;
            HealthCoefficient = improvementRate;
            ArmorCoefficient = improvementRate;
            SpeedCoefficient = improvementRate;
            DamageCoefficient = improvementRate;
        }

        public Rank(string name, double healthCoefficient, double armorCoefficient, double speedCoefficient, double damageCoefficient)
        {
            _name = name;
            HealthCoefficient = healthCoefficient;
            ArmorCoefficient = armorCoefficient;
            SpeedCoefficient = speedCoefficient;
            DamageCoefficient = damageCoefficient;
        }

        public override string ToString()
        {
            return _name;
        }
    }
}
