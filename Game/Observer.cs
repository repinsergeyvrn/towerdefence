﻿using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace towerDefense
{
    class Observer
    {

        // издатели, те объекты которые содержат важное событие, 
        // в нашем случае издатели это игровые объекты
        // издатели хранят список подписчиков
        // событиями будет изменение положения, и изменение здоровья

        // подписчики отселживают события, в нашем случае это консоль
        // сделаю класс который будет формировать представление на консоль
        // он будет рисовать информацию на консоль
        // его дергают игровые объекты
        // он хранит текущее состояние, и вносит в него изменения
        // 



        public Observer()
        {
            Console.CursorVisible = false;

            Console.WriteLine("Первая строка");
            Console.WriteLine("Вторая строка");
            Console.WriteLine("Третья строка");
            bool fl = false;
            while (true)
            {
                if (fl)
                {
                    Console.SetCursorPosition(0, 0);
                    Console.Write("Первая");
                    Console.SetCursorPosition(0, 1);
                    Console.Write("Вторая");
                    Console.SetCursorPosition(0, 2);
                    Console.Write("Третья");
                    fl = false;
                }
                else
                {
                    Console.SetCursorPosition(0, 0);
                    Console.Write("Третья");
                    Console.SetCursorPosition(0, 1);
                    Console.Write("Вторая");
                    Console.SetCursorPosition(0, 2);
                    Console.Write("Первая");
                    fl = true;
                }
                Thread.Sleep(10);
                //Console.MoveBufferArea(0, 0);
            }
        }
    }
    
    public class Subscriber
    {
        private Timer _timeRender;
        private LinkedBufferInfo _buffer;
        private int _width;
        private int _height;
        private int _countPublisher;
        
        public Subscriber()
        {
            _timeRender = new Timer(new TimerCallback(Render));
            _timeRender.Change(0, 100);
            _width = Console.WindowWidth;
            _height = Console.WindowHeight;
            _buffer = new LinkedBufferInfo();
            _countPublisher = 0;
            Console.CursorVisible = false;
        }

        public void IAmUpdate(Unit unit)
        {
            if (!_buffer.Update(unit))
            {
                _buffer.Add(unit);
                _countPublisher++;
            }
        }

        public void DeleteInfo(Unit unit)
        {
            _buffer.Remove(unit);
            _countPublisher--;
            ClearConsole();
        }

        private void ClearConsole()
        {
            for (int i = _countPublisher; i < _height; i++)
            {
                PrintInfoConsole(i, "");
            }
        }

        private void PrintInfoConsole(int numberString, string info)
        {
            Console.SetCursorPosition(0, numberString);
            Console.Write(info);
            for (int i = info.Length; i < _width; i++)
            {
                Console.SetCursorPosition(i, numberString);
                Console.Write(" ");
            }
        }

        public void Render(object timerState)// https://refactoring.guru/ru/design-patterns/observer
        {
            BufferInfo current = null;
            if (_buffer != null)
            {
                current = _buffer.Head;
            }
            int stringNumber = 0;
            while (current != null)
            {
                if (current.Update)
                {
                    PrintInfoConsole(stringNumber, current.Info);
                    current.Update = false;
                }
                stringNumber++;
                current = current.Next;
            }
            PrintInfoConsole(stringNumber, "Для добавления юнита нажмите Z");
            //Console.WriteLine("")
        }
    }
    
    class BufferInfo
    {
        public bool Update { get; set; } = true;
        public string Info { get; set; } = "";
        public Unit Unit { get; set; }
        public BufferInfo Next { get; set; }

        public BufferInfo(string info)
        {
            Info = info;
        }

        public BufferInfo(Unit unit)
        {
            Unit = unit;
            Info = unit.Info();
        }
    }

    class LinkedBufferInfo // TO DO реализовать енумератор
    {
        BufferInfo head;
        BufferInfo tail;
        int count;

        public void Add(Unit unit)
        {
            BufferInfo node = new BufferInfo(unit);

            if (head == null)
                head = node;
            else
                tail.Next = node;
            tail = node;

            count++;
        }

        // удаление элемента
        public bool Remove(Unit unit)
        {
            BufferInfo current = head;
            BufferInfo previous = null;
            while (current != null)
            {
                if (current.Unit == unit)
                {
                    if (previous != null)
                    {
                        previous.Next = current.Next;

                        if (current.Next == null)
                            tail = previous;
                    }
                    else
                    {
                        head = head.Next;

                        if (head == null)
                            tail = null;
                    }

                    count--;
                    return true;
                }
                previous = current;
                current = current.Next;

            }
            return false;
        }

        public bool Update(Unit unit)
        {
            BufferInfo current = head;
            while (current != null)
            {
                if (current.Unit.Equals(unit))
                {
                    current.Info = unit.Info();
                    current.Update = true;
                    return true;
                }
                current = current.Next;
            }
            return false;
        }

        public int Count { get { return count; } }

        internal BufferInfo Head { get => head; }
    }
}
