﻿using System.Drawing;

namespace towerDefense
{
    public class Fraction : ICrete
    {
        string _name;
        int _baseHealth;
        int _baseArmor;
        Position _position;
        Point _direction;

        public Fraction(string name, int baseHealth, int baseArmor, Position position)
        {
            _name = name;
            _baseHealth = baseHealth;
            _baseArmor = baseArmor;
            _position = position;
        }

        public Fraction(string name, int baseHealth, int baseArmor, Position position, Point direction):
            this(name, baseHealth, baseArmor, position)
        {
            _direction = direction;
        }

        public Point Direction { get => _direction; }

        public void ApplayCharacteristics(Unit Unit)
        {
            Unit.Health = _baseHealth;
            Unit.Armor  = _baseArmor;
            Unit.Position = (Position)_position.Clone();
        }

        public override string ToString()
        {
            return _name;
        }
    }
}
