﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace towerDefense
{
    public interface IGameContext : IGameContextCommand, IGameContextHostileUnits
    {
        public Subscriber Console { get; set; }
        public List<Unit> AllUnits { get; }
        public List<AvailableUnit> AvailableUnits { get; set; }

        void AddUnit(Unit unit);
        void RemoveUnit(Unit unit);
        int RemoveAll(Predicate<Unit> match);        
    }
}