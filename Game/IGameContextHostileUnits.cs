﻿using System.Collections.Generic;

namespace towerDefense
{
    public interface IGameContextHostileUnits
    {
        List<SortedSet<Unit>> HostileUnits(Fraction ownFraction);
        Unit NearHostileUnit(Fraction ownFraction, System.Drawing.Point ownDirection);
        void UpdatePositionRemove(Unit unit);
        void UpdatePositionAdd(Unit unit);
    }
}
