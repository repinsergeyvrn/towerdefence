﻿using System;


namespace towerDefense
{
    public interface IGameContextCommand
    {
        void AddDeferredCommand(Command command, TimeSpan leadTime);
        void BuyUnit(int numbers);
    }
}
