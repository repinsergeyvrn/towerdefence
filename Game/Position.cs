﻿using System;

namespace towerDefense
{
    public class Position: ICloneable
    {
        private double _positionX;
        private double _positionY;

        public double X
        {
            get => _positionX;
            set => _positionX = value;
        }

        public double Y
        {
            get => _positionY;
            set => _positionY = value;
        }

        public Position()
        {

        }

        public Position(double X, double Y)
        {
            _positionX = X;
            _positionY = Y;

        }

        public void AddIs(Position offset)
        {
            _positionX = _positionX + offset._positionX;
            _positionX = _positionY + offset._positionY;
        }

        public void AddIs(double offsetX, double offsetY)
        {
            _positionX = _positionX + offsetX;
            _positionY = _positionY + offsetY;
        }


        public Position Add(Position offset)
        {
            return new Position(_positionX + offset._positionX, _positionY + offset._positionY);
        }

        public Position Add(double offsetX, double offsetY)
        {
            return new Position(_positionX + offsetX, _positionY + offsetY);
        }

        public Position Copy() => new Position(_positionX, _positionY);

        public override string ToString()
        {
            return string.Format("[{0:N3} , {1:N3}]", _positionX, _positionY);
        }

        public static double Distance(Position P1, Position P2)
        {
            return Math.Sqrt(Math.Pow(P2.X - P1.X, 2) + Math.Pow(P2.Y - P1.Y, 2));
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public override bool Equals(object obj)
        {
            return obj is Position position &&
                   _positionX == position._positionX &&
                   _positionY == position._positionY;
        }
    }


}
