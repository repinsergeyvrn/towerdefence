﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace towerDefense
{
    class UnitComparer : IComparer<Unit>
    {
        public int Compare(Unit x, Unit y)
        {
            if (x.Equals(y))
            {
                return 0;
            }
            if (x.Position.X < y.Position.X)
            {
                return -1;
            }
            else
            {
                return 1;
            }
        }
    }
}
