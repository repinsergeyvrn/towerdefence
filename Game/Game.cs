using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;

namespace towerDefense
{
    class Game
    {
        private Stopwatch _inGameTime;
        private Stopwatch _inCycleTime;
        Subscriber console = new Subscriber();
        GameContext GC;
        TimeSpan tick;
        //List<DeferredCommand> _deferredCommands;
        //private Timer _timeRender;
        public Game()
        {
            Position leftFortressPosition = new(-30, 0);
            Position rightFortressPosition = new(30, 0);

            Point rightDirection = new Point(1, 0);
            Point leftDirection = new Point(-1, 0);

            Fraction Mags = new Fraction("Маги", 100, 100, leftFortressPosition, rightDirection);
            Fraction Hunters = new Fraction("Охотники", 100, 0, rightFortressPosition, leftDirection);

            ////Ранги
            Rank Base = new Rank("Базовый", 0);
            Rank Soldier = new Rank("Солдат", 1);
            Rank Sergeant = new Rank("Сержант", 3);
            Rank Officer = new Rank("Офицер", 5);

            GC = new GameContext();
            GC.Console = console;

            //коллизии, дистанция атаки, урон
            FixedUnit fix1 = new FixedUnit(Mags, Officer, 5, 3, 30);
            FixedUnit fix2 = new FixedUnit(Hunters, Officer, 5, 3, 30);
            GC.AddUnit(fix1);
            fix1.Health = 5000;
            console.IAmUpdate(fix1);
            GC.AddUnit(fix2);
            fix2.Health = 5000;
            console.IAmUpdate(fix2);

            GC.AvailableUnits.Add(new AvailableUnit("Создать базового мага", GC, Mags, Base,     100)); // 0
            GC.AvailableUnits.Add(new AvailableUnit("Создать мага солдата",  GC, Mags, Soldier,  500)); // 1
            GC.AvailableUnits.Add(new AvailableUnit("Создать мага сержанта", GC, Mags, Sergeant, 1000));// 2
            GC.AvailableUnits.Add(new AvailableUnit("Создать мага офицера",  GC, Mags, Officer,  3000));// 3

            GC.AvailableUnits.Add(new AvailableUnit("Создать базового мага", GC, Hunters, Base,     100)); // 4
            GC.AvailableUnits.Add(new AvailableUnit("Создать мага солдата" , GC, Hunters, Soldier,  500)); // 5
            GC.AvailableUnits.Add(new AvailableUnit("Создать мага сержанта", GC, Hunters, Sergeant, 1000));// 6
            GC.AvailableUnits.Add(new AvailableUnit("Создать мага офицера" , GC, Hunters, Officer,  3000));// 7


            //AddMobileUnit addMobileMagSoldier = new AddMobileUnit(GC, Mags, Soldier);
            //GC.AvailableUnits[0].Command.Execute();
            GC.BuyUnit(1);

            
            GC.BuyUnit(7);

            GC.BuyUnit(7);
            GC.BuyUnit(7);
            GC.BuyUnit(7);
            GC.BuyUnit(7);

            for (int i = 0; i < 5; i++)
            {
                GC.BuyUnit(1);
            }

            AddMobileUnit addMobileHunerOfficer = new AddMobileUnit(GC, Hunters, Officer);
            GC.AddDeferredCommand(addMobileHunerOfficer, TimeSpan.FromSeconds(5));
            GC.AddDeferredCommand(addMobileHunerOfficer, TimeSpan.FromSeconds(6));
            GC.AddDeferredCommand(addMobileHunerOfficer, TimeSpan.FromSeconds(7));
            GC.AddDeferredCommand(addMobileHunerOfficer, TimeSpan.FromSeconds(8));
        }

        public void Start()
        {
            _inGameTime = new Stopwatch();
            _inGameTime.Start();
            _inCycleTime = new Stopwatch();
            _inCycleTime.Start();

            while (true)
            {
                Update();
            }
        }

        public void Stop()
        {

        }

        private void Update()
        {

            _inCycleTime.Restart();

            foreach (var item in GC.AllUnits)
            {
                item.ObjectUpdate(tick, GC);
            }
            tick = _inCycleTime.Elapsed;

            //GC.RemoveAll(item => item.Health <= 0);

            foreach (var item in GC.DeferredCommand)
            {
                item.AddAccumulatedTime(tick);
                if (item.CanExecute)
                    item.Command.Execute();

            }
            GC.DeferredCommand.RemoveAll(item => item.CanExecute);
            //CommandFromFile();

        }


        private void CreateAvailabelList()
        {
        
        }

        private void ExecuteCommand(Command command)
        {
            command.Execute();
        }


        void CommandFromFile()
        {
            string line;
            StreamReader sr = new StreamReader("TestFile.txt");
            line = sr.ReadLine();
            if (line != "")
            {
                //Вызов создания юнитов
            }
            sr.Close();

            StreamWriter sw = new StreamWriter("TestFile.txt", false);
            sw.WriteLine("");
            sw.Close();

        }


    }
}