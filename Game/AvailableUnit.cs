﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace towerDefense
{
    public class AvailableUnit
    {
        public string Name { get; }
        public Command Command { get; }
        public int Cost { get; }

        public AvailableUnit(string name, GameContext gameContext, Fraction fraction, Rank rank, int cost)
        {
            this.Name = name;
            this.Command = new AddMobileUnit(gameContext, fraction, rank);
            this.Cost = cost;
        }
    }
}
