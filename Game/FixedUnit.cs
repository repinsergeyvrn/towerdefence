﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace towerDefense
{
    class FixedUnit : AttackUnit
    {
        public FixedUnit(Fraction fraction, Rank rank)
        {
            _fraction = fraction;
            _rank = rank;

            fraction.ApplayCharacteristics(this);
            _damage = (int)Math.Round(_rank.DamageCoefficient * _damage, MidpointRounding.AwayFromZero);
            base.AplayRank();
        }

        public FixedUnit(Fraction fraction, Rank rank, double collisionDistance)
            : this(fraction, rank)
        {
            _collisionDistance = collisionDistance;
        }

        public FixedUnit(Fraction fraction, Rank rank, double collisionDistance, double attackDistance, int damage)
            : this(fraction, rank, collisionDistance)
        {
            _attackDistance = attackDistance;
            _damage = damage;
        }

        public override void ObjectUpdate(TimeSpan timeSpan, IGameContext gameContext)
        {
            CalculateDamage(gameContext);
            _betweenAttak = _betweenAttak.Add(timeSpan);
            _canAttack = _betweenAttak > _timeAttac ? true : false;
            CalculateCollision(gameContext);
            Attack();
        }

        private void CalculateCollision(IGameContext gameContext)
        {
            _attackUnit.Clear();
            foreach (var item in gameContext.AllUnits)
            {
                if (item.Fraction == Fraction)
                {
                    continue;
                }
                else
                {
                    double distance = Position.Distance(this.Position, item.Position);
                    if (_canAttack && distance <= _attackDistance)
                    {
                        _attackUnit.Add((AttackUnit)item);
                    }
                }
            }
        }
    }
}
