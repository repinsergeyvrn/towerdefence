﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace towerDefense
{
    interface IUnit
    {
        void ObjectUpdate(TimeSpan timeSpan, IGameContext gameContext);
    }
}
