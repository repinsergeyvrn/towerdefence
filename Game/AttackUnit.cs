﻿using System;
using System.Collections.Generic;

namespace towerDefense
{
    public abstract class AttackUnit : Unit, IAttack
    {
        private int _accumulatedDamage = 0;

        protected bool _canAttack = false;
        protected int _damage = 10;
        protected TimeSpan _timeAttac = TimeSpan.FromSeconds(1);
        protected TimeSpan _betweenAttak = default;
        protected List<AttackUnit> _attackUnit = new();
        protected double _attackDistance = 1;

        public int AccumulatedDamage { get => _accumulatedDamage; set => _accumulatedDamage = value; }
        

        public void Attack()
        {
            bool wasAttack = false;
            foreach (var item in _attackUnit)
            {
                item.AccumulatedDamage += _damage;
                wasAttack = true;

            }
            if (wasAttack)
            {
                _betweenAttak = default;
                _canAttack = false;
            }
        }

        public void CalculateDamage(IGameContext gameContext)
        {
            
            _health-= AccumulatedDamage;

            if (_accumulatedDamage > 0)
            {
                gameContext.Console.IAmUpdate((Unit)this);
            }

            if (_health <= 0)
            {
                gameContext.Console.DeleteInfo((Unit)this);
            }
            AccumulatedDamage = 0;
        }
    }
}
