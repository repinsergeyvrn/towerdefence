﻿using System;
using System.Collections.Generic;

namespace towerDefense
{
    interface IAttack
    { 
        void Attack();
        void CalculateDamage(IGameContext gameContext);
    }
}
