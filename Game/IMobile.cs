﻿using System;
using System.Collections.Generic;

namespace towerDefense
{
    public interface IMobile
    {
        void CalculatePosition(TimeSpan timeSpan, IGameContext gameContext);
        double Speed { get; set; }
    }
}
