using System;
using System.Drawing;
using towerDefense;
using Xunit;

namespace Testing
{
    public class MoveTest
    {
        GameContext gameContext = new GameContext();
        Position basePosition = new Position(0, 0);
        int baseHealth = 100;
        int baseArmor = 0;
        
        [Fact]
        public void RightMove()
        {
            // Arrange
            Point rightDirection = new Point(1, 0);
            Fraction testFraction = new Fraction("�������� �������", baseHealth, baseArmor, basePosition, rightDirection);
            Rank testRank = new Rank("�������� ����", 0);
            MobileUnit mobileUnit = new MobileUnit(testFraction, testRank, rightDirection);

            // Act
            mobileUnit.CalculatePosition(TimeSpan.FromSeconds(1), gameContext);

            // Assert
            var expectedPosition = new Position(1, 0);
            Assert.Equal(expectedPosition, mobileUnit.Position);
        }

        [Fact]
        public void LefttMove()
        {
            // Arrange
            Point leftDerection = new Point(-1, 0);
            Fraction testFraction = new Fraction("�������� �������", baseHealth, baseArmor, basePosition);
            Rank testRank = new Rank("�������� ����", 0);
            MobileUnit mobileUnit = new MobileUnit(testFraction, testRank, leftDerection);

            // Act
            mobileUnit.CalculatePosition(TimeSpan.FromSeconds(1), gameContext);

            // Assert
            var expectedPosition = new Position(-1, 0);
            Assert.Equal(expectedPosition, mobileUnit.Position);
        }

        [Fact]
        public void FastRightMove()
        {
            // Arrange
            Point rightDirection = new Point(1, 0);
            Fraction testFraction = new Fraction("�������� �������", baseHealth, baseArmor, basePosition, rightDirection);
            Rank testRank = new Rank("�������� ����", 2);
            MobileUnit mobileUnit = new MobileUnit(testFraction, testRank, rightDirection);

            // Act
            mobileUnit.CalculatePosition(TimeSpan.FromSeconds(1), gameContext);

            // Assert
            var expectedPosition = new Position(3, 0);
            Assert.Equal(expectedPosition, mobileUnit.Position);
        }

        [Fact]
        public void FastLefttMove()
        {
            // Arrange
            Point leftDerection = new Point(-1, 0);
            Fraction testFraction = new Fraction("�������� �������", baseHealth, baseArmor, basePosition);
            Rank testRank = new Rank("�������� ����", 2);
            MobileUnit mobileUnit = new MobileUnit(testFraction, testRank, leftDerection);

            // Act
            mobileUnit.CalculatePosition(TimeSpan.FromSeconds(1), gameContext);

            // Assert
            var expectedPosition = new Position(-3, 0);
            Assert.Equal(expectedPosition, mobileUnit.Position);
        }
    }

    
}
