﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Moq;
using towerDefense;

namespace Testing.Unit
{
    public class Move
    {

        System.Drawing.Point direction;
        Fraction fraction;
        Rank rank;

        public Move()
        {
            // Arrange
            direction = new System.Drawing.Point(-1, 0);
            fraction = new Fraction("Тест", 100, 0, new Position(10, 0));
            rank = new Rank("Тест", 1);
        }

        [Fact]
        public void CalculatePositionCollision()
        {
            // Arrange
            var MU = new MobileUnit(fraction, rank, direction);
            IGameContext gameContext = Mock.Of<IGameContext>(_ => _.NearHostileUnit(fraction, direction) == GetTestHostileUnit());

            //Act
            MU.CalculatePosition(TimeSpan.FromSeconds(50), gameContext);

            //Assert
            Assert.Equal(new Position(-9, 0).ToString(), MU.Position.ToString());
        }

        [Fact]
        public void CalculatePositionNotCollision()
        {
            // Arrange
            var MU = new MobileUnit(fraction, rank, direction);
            IGameContext gameContext = Mock.Of<IGameContext>(_ => _.NearHostileUnit(fraction, direction) == GetTestHostileUnit());

            //Act
            MU.CalculatePosition(TimeSpan.FromSeconds(1), gameContext);

            //Assert
            Assert.Equal(new Position(8, 0).ToString(), MU.Position.ToString());
        }

        private MobileUnit GetTestHostileUnit()
        {
            var fraction = new Fraction("Тест", 100, 0, new Position(-10, 0));
            var rank = new Rank("adsf", 1);
            return new MobileUnit(fraction, rank);
        }

    }
}
