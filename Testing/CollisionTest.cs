﻿using System;
using System.Collections.Generic;
using System.Drawing;
using towerDefense;
using Xunit;

namespace Testing
{
    public class CollisionTest
    {
        GameContext gameContext = new GameContext();
        
        int baseHealth = 100;
        int baseArmor = 0;


        [Fact]
        public void Collision()
        {
            // Arrange
            Rank testRank = new Rank("Тестовый ранг", 0);

            Position leftPosition = new Position(-5, 0);
            Point rightDirection = new Point(1, 0);
            Fraction leftFraction = new Fraction("Тестовая фракция 1", baseHealth, baseArmor, leftPosition, rightDirection);
            MobileUnit LeftMobileUnit = new MobileUnit(leftFraction, testRank, rightDirection);

            Position rightPosition = new Position(5, 0);
            Point leftDirection = new Point(-1, 0);
            Fraction rightFraction = new Fraction("Тестовая фракция 1", baseHealth, baseArmor, rightPosition, leftDirection);
            MobileUnit RightMobileUnit = new MobileUnit(rightFraction, testRank, leftDirection);

            List<MobileUnit> mobileUnits = new List<MobileUnit>();
            mobileUnits.Add(LeftMobileUnit);
            mobileUnits.Add(RightMobileUnit);

            // Act
            for (int i = 0; i < 100; i++)
            {
                foreach (var item in mobileUnits)
                {
                    item.CalculateCollision(gameContext);
                    item.CalculatePosition(TimeSpan.FromSeconds(0.1), gameContext);
                }
            }

            // Assert
            Assert.Equal(new Position(-0.5, 0).ToString(), LeftMobileUnit.Position.ToString());
            Assert.Equal(new Position(0.5, 0).ToString(), RightMobileUnit.Position.ToString());
        }

        [Fact]
        public void DelayCollision()
        {
            // Arrange
            Rank testRank = new Rank("Тестовый ранг", 0);

            Position leftPosition = new Position(-5, 0);
            Point rightDirection = new Point(1, 0);
            Fraction leftFraction = new Fraction("Тестовая фракция 1", baseHealth, baseArmor, leftPosition, rightDirection);
            MobileUnit LeftMobileUnit = new MobileUnit(leftFraction, testRank, rightDirection);

            Position rightPosition = new Position(5, 0);
            Point leftDirection = new Point(-1, 0);
            Fraction rightFraction = new Fraction("Тестовая фракция 1", baseHealth, baseArmor, rightPosition, leftDirection);
            MobileUnit RightMobileUnit = new MobileUnit(rightFraction, testRank, leftDirection);


            gameContext.AddUnit(LeftMobileUnit);
            gameContext.AddUnit(RightMobileUnit);

            List<MobileUnit> mobileUnits = new List<MobileUnit>();
            mobileUnits.Add(LeftMobileUnit);
            mobileUnits.Add(RightMobileUnit);

            // Act
            LeftMobileUnit.CalculateCollision(gameContext);
            LeftMobileUnit.CalculatePosition(TimeSpan.FromSeconds(20), gameContext);


            // Assert
            Assert.Equal(new Position(4, 0).ToString(), LeftMobileUnit.Position.ToString());
            Assert.Equal(new Position(5, 0).ToString(), RightMobileUnit.Position.ToString());

        }
    }
}
