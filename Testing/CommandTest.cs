﻿using System;
using System.Collections.Generic;
using Xunit;
using towerDefense;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testing
{
    public class CommandTest
    {
        int baseHealth = 100;
        int baseArmor = 0;


        [Fact]
        public void AddCommand()
        {
            // Arrange
            GameContext gameContext = new GameContext();
            Rank testRank = new Rank("Тестовый ранг", 0);

            Position leftPosition = new Position(-5, 0);
            Point rightDirection = new Point(1, 0);
            Fraction leftFraction = new Fraction("Тестовая фракция 1", baseHealth, baseArmor, leftPosition, rightDirection);
            AddMobileUnit addMobileLeft = new AddMobileUnit(gameContext, leftFraction, testRank);

            Position rightPosition = new Position(5, 0);
            Point leftDirection = new Point(-1, 0);
            Fraction rightFraction = new Fraction("Тестовая фракция 1", baseHealth, baseArmor, rightPosition, leftDirection);
            AddMobileUnit addMobileRight = new AddMobileUnit(gameContext, rightFraction, testRank);

            // Act
            //ExecuteCommand(addMobileLeft);
            //ExecuteCommand(addMobileRight);

            // Assert


            MobileUnit LeftMobileUnit = new MobileUnit(leftFraction, testRank, rightDirection);
            MobileUnit RightMobileUnit = new MobileUnit(rightFraction, testRank, leftDirection);


            //Assert.Equal(2, gameContext.AllUnits.Count);
            //Assert.Equal(1, gameContext.HostileUnits(leftFraction)[0].Count); // Надо бы умнее проверять
            //Assert.Equal(1, gameContext.HostileUnits(rightFraction)[0].Count); // надо бы умнее проверять
        }

        private void ExecuteCommand(towerDefense.Command command)
        {
            command.Execute();
        }
    }
}
