﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Xunit;
using towerDefense;

namespace Testing.Command
{
    public class GameCommand
    {
        System.Drawing.Point direction;
        Fraction fraction;
        Rank rank;

        public GameCommand()
        {
            // Arrange
            //direction = new System.Drawing.Point(-1, 0);
            fraction = new Fraction("Тест", 100, 0, new Position(10, 0));
            rank = new Rank("Тест", 1);
        }

        [Fact]
        public void AddUnit()
        {
            // Arrange
            var MobilleUnit = new MobileUnit(fraction, rank, direction);
            //_gameContext.AddUnit(new MobileUnit(_fraction, _rank, _fraction.Direction))
            Mock<IGameContext> gameContext = new Mock<IGameContext>();
            gameContext.Setup(_ => _.AddUnit(It.IsAny<MobileUnit>())).Verifiable();
            AddMobileUnit addMobileUnit = new AddMobileUnit(gameContext.Object, fraction, rank);

            //Act
            ExecuteCommand(addMobileUnit);// MobilleUnit.CalculatePosition(TimeSpan.FromSeconds(50), gameContext);

            //Assert
            gameContext.Verify();
        }

        private void ExecuteCommand(towerDefense.Command command)
        {
            command.Execute();
        }
    }
}
